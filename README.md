# **Python - PBR Textures UI**

**This is a Blender addon written in Python, which adds a simplified UI for handling PBR Textures.**

##  **Features**

### **Supported Input Textures**
-   Base Color
-   Ambient Occlusion
-   Roughness
-   Normal
-   Height
-   (soon) Opacity
-   (soon) Metallic

### **Simple User Interface**
Many parts of the workflow of creating PBR textures from images are an annoying process and settings are scattered around multiple tabs in blender.

My PBR UI took all connections between Image Textures, Displacements, and ShaderNodes and wires it all together, while the only thing you need to do is to import the 5 textures.
Optionally you can use all kinds of settings to initially give your material the correct look.

### **Recommended Workflow**
You can apply PBR Textures in 2 ways. You can either create a new material or you can override existing materials.
While its possible to only override a material with 1 input texture or to create a new material with 2, 3, 4, 1 or none input textures, you should still think about using those 2 modes for different reasons.

When creating a PBR Texture the first time, use 'create new'.
If you see that some bump or displacement values are off, or if you have another texture you'd like to try out (for example another base texture but keep the rest), then you rather should use the Override active material function. Technicially it comes down to common sense.


## **Frequently Asked Questions**

### **How to run this script**

**1.**      Download this Script and save it somewhere

**2.**      Open Blender

**3.**      Switch to the Scripting Tab.
            The scripting Tab will contain a text-editor. At the top of 
            the text editor you will find settings to create a 'new' 
            textfile and to 'open' a textfile. 

**4.**      'Open' this Script from the menu at the top of the 
Text Editor

**5.**      Click on the Play Button to run the Script

### **How does this script execute**
Its source code will be compiled onto your current .blend file, which results in you having an additional Panel in your 3D Viewport.

### **Where can i find the UI that this script creates**
The new UI can be found at the top right corner of your 3D Viewport, **while an object is selected**.
Normally the top right Corner only hosts 3 tabs 'Item', 'Tool' and 'View'. The script adds a fourth Tab called 'Scripts'.
The PBR UI can be found in the 'Scripts' Tag.

