# ------------------------------------------------------------------------
#    Import Libraries
# ------------------------------------------------------------------------

import bpy
import os 
from bpy.types import Panel, Operator
from bpy_extras.io_utils import ImportHelper 
from bpy.props import StringProperty, BoolProperty 


# ------------------------------------------------------------------------
#    General Prototypes
# ------------------------------------------------------------------------

def doesNotExist(param):
    if param is not None:
        return False
    else:
        return True
    
def loadExampleImages():
    base_directory = "C:\\Development\\gamedev\\blender\\materials\\Pebbels"
    material_name = "\\Pebbles_023_"
    filenames = ["BaseColor.jpg", "AmbientOcclusion.jpg", "Roughness.jpg", "Normal.jpg", "Height.png"]
    merged_paths = []
    for file_name in filenames:
        path = base_directory + material_name + file_name
        merged_paths.append(path)
    Global.inputImages = {"base_color": merged_paths[0], "ambient_occlusion": merged_paths[1], "roughness": merged_paths[2], "normal": merged_paths[3], "height": merged_paths[4], "opacity": "unassigned", "metallic": "unassigned"}
    
def printList(list):
    for k, v in list.items():
        print("key: "+k + " | value: "+v)
    return

def addList(list, newKey, newValue):
    list[newKey] = newValue
    return list

def getListItemValue(list, index):
    loopIndex = 0
    value = "unassigned"
    for k, v in list.items():
        if index == loopIndex:
            value = v
            break
        loopIndex = loopIndex + 1
    return value

def getListItemKey(list, index):
    loopIndex = 0
    key = "unassigned"
    for k, v in list.items():
        if index == loopIndex:
            key = k
            break
        loopIndex = loopIndex + 1
    return key

# ------------------------------------------------------------------------
#    Global Classes
# ------------------------------------------------------------------------

class Global:

    # Input Images
    inputImages = {"base_color": "unassigned", "ambient_occlusion": "unassigned", "roughness": "unassigned", "normal": "unassigned", "height": "unassigned", "opacity": "unassigned", "metallic": "unassigned"}
    inputImageID = 0

    # UI Attributes
    panelName = "PBR Replacer"
    submitButtonName = "Create new Material"

    # Global Properties
    scale = (1.0, 1.0, 1.0)
    subdivisionLevel = 7
    addMaterialToObject = 1

    # Node Positioning
    highestNodePosition = 1200
    nodeHeightOffset = 300
    mostLeftNodePosition = -2000
    nodeWidthOffset = 400


# ------------------------------------------------------------------------
#    Operators
# ------------------------------------------------------------------------


class OT_TestOpenFileBrowser(Operator, ImportHelper): 
    bl_idname = "test.open_filebrowser" 
    bl_label = "Open the file browser" 

    filter_glob: StringProperty( 
        default='*.jpg;*.jpeg;*.png;*.tif;*.tiff;*.bmp', options={'HIDDEN'} 
    ) 

    def execute(self, context): 
        """Do something with the selected file(s).""" 
        filename = "unassigned"
        filename, extension = os.path.splitext(self.filepath)
        if filename != "unassigned": 
            addList(Global.inputImages, getListItemKey(Global.inputImages, Global.inputImageID), filename + extension)
        print(filename)    
        return {'FINISHED'}



class uploadOperator(Operator):
    bl_idname = "object.upload_operator"
    bl_label = "UploadOperator"
    id: bpy.props.IntProperty(default=0)
    
    def execute(self, context):
        Global.inputImageID = self.id
        bpy.ops.test.open_filebrowser('INVOKE_DEFAULT')
        return {'FINISHED'}
    

class resetOperator(Operator):
    bl_idname = "object.reset_operator"
    bl_label = "ResetOperator"
    id: bpy.props.IntProperty(default=0)

    def execute(self, context):
        print("Hello World")
        addList(Global.inputImages, getListItemKey(Global.inputImages, self.id), "unassigned")
        return {'FINISHED'}
    


class submitOperator(Operator):
    bl_idname = "object.submit_operator"
    bl_label = "Submit Operator"
    id: bpy.props.IntProperty(default=0)

    def execute(self, context):

        #loadExampleImages();

        object = context.object;
        



        if self.id == 0: 
            activeMaterial = bpy.data.materials.new(context.scene.material_name_prop);
        
        if self.id == 1:
            activeMaterial = context.object.active_material
        

        activeMaterial.use_nodes = True


        node_tree = activeMaterial.node_tree
        nodes = activeMaterial.node_tree.nodes
        bsdfNode = nodes["Principled BSDF"] 


        
        imageReferenceID = 0
        imageTextureNodes = {"base_color": "unassigned", "ambient_occlusion": "unassigned", "roughness": "unassigned", "normal": "unassigned", "height": "unassigned", "opacity": "unassigned", "metallic": "unassigned"}
        imageInstances = []


        # 
        for inputImageKey, inputImageValue  in Global.inputImages.items():
            #
            if(inputImageValue == "unassigned"):
                addList(imageTextureNodes, inputImageKey, "unassigned")
                imageReferenceID = imageReferenceID+1
                continue
            
            # Create ShaderNode for this TextureImage and assign 
            if self.id == 0 or doesNotExist(nodes.get(inputImageKey, None)):
                imageTextureNode = nodes.new("ShaderNodeTexImage")
            else:
                if nodes[inputImageKey]:
                    imageTextureNode = nodes[inputImageKey]
                else:
                    imageTextureNode = nodes.new("ShaderNodeTexImage")
                   

            imageTextureNode.location = ((Global.mostLeftNodePosition + (Global.nodeWidthOffset * 2)), Global.highestNodePosition - (Global.nodeHeightOffset * imageReferenceID))
            
            # Load External Image to Variable
            imageInstanced = bpy.data.images.load(inputImageValue)
            imageInstanced.name = inputImageKey
            imageInstances.append(imageInstanced)

            imageTextureNode.image = imageInstanced
            imageTextureNode.name = inputImageKey

            addList(imageTextureNodes, inputImageKey, imageTextureNode)
            
            imageReferenceID = imageReferenceID+1
            
        #   -----------------------------------------------------------------------------
        #   Node Tree 
        #   -----------------------------------------------------------------------------

        
        # Displacement
        if context.scene.use_displacement_modifier_prop:
            if self.id == 0 or doesNotExist(object.modifiers.get("Displace", None)):
                displaceTexture = bpy.data.textures.new("DisplaceTexture", "IMAGE")
            else: 
                displaceTexture = bpy.data.textures["DisplaceTexture"]

            if imageTextureNodes["height"] != "unassigned":
                displaceTexture.image = imageTextureNodes["height"].image # id of height texture but as image instance


        #   -----------------------------------------------------------------------------
        #   Node Tree 
        #   -----------------------------------------------------------------------------
        
        textureCoordinatesNode = None

        # Create Nodes

        if (imageTextureNodes["base_color"] != "unassigned" or imageTextureNodes["ambient_occlusion"] != "unassigned" or imageTextureNodes["roughness"] != "unassigned" or imageTextureNodes["normal"] != "unassigned" or imageTextureNodes["height"] != "unassigned"):
            if self.id == 0 or doesNotExist(nodes.get("Texture Coordinate", None)):
                textureCoordinatesNode = nodes.new("ShaderNodeTexCoord")
            else:
                textureCoordinatesNode = nodes["Texture Coordinate"]
            textureCoordinatesNode.location = ((Global.mostLeftNodePosition + (Global.nodeWidthOffset * 0)), Global.highestNodePosition - (Global.nodeHeightOffset * 0))

            if self.id == 0 or doesNotExist(nodes.get("Mapping", None)):
                vectorMappingNode = nodes.new("ShaderNodeMapping")
            else:
                vectorMappingNode = nodes["Mapping"]
            vectorMappingNode.location = ((Global.mostLeftNodePosition + (Global.nodeWidthOffset * 1)), Global.highestNodePosition - (Global.nodeHeightOffset * 0))
            vectorMappingNode.inputs["Scale"].default_value = context.scene.scale_vectors_prop

            if imageTextureNodes["base_color"] != "unassigned" and imageTextureNodes["ambient_occlusion"] != "unassigned" :
                if self.id == 0 or doesNotExist(nodes.get("Mix", None)):
                    mixRGBNode = nodes.new("ShaderNodeMixRGB")
                else:
                    mixRGBNode = nodes["Mix"]
                mixRGBNode.location = ((Global.mostLeftNodePosition + (Global.nodeWidthOffset * 3)), Global.highestNodePosition - (Global.nodeHeightOffset * 0))
                mixRGBNode.blend_type = "MULTIPLY"

            if imageTextureNodes["normal"] != "unassigned" :
                if self.id == 0 or doesNotExist(nodes.get("Bump", None)):
                    bumpNode = nodes.new("ShaderNodeBump")
                else:
                    bumpNode = nodes["Bump"]
                bumpNode.location = ((Global.mostLeftNodePosition + (Global.nodeWidthOffset * 3)), Global.highestNodePosition - (Global.nodeHeightOffset * 6))
                bumpNode.inputs["Strength"].default_value = context.scene.bump_strength_prop

            if imageTextureNodes["roughness"] != "unassigned" :
                if self.id == 0 or doesNotExist(nodes.get("Gamma", None)):
                    gammaNode = nodes.new("ShaderNodeGamma")
                else:
                    gammaNode = nodes["Gamma"]
                gammaNode.location = ((Global.mostLeftNodePosition + (Global.nodeWidthOffset * 3)), Global.highestNodePosition - (Global.nodeHeightOffset * 2))
                gammaNode.inputs["Gamma"].default_value = context.scene.roughness_gamma_prop




        #   -----------------------------------------------------------------------------
        #   All node Tree Links Including alternative Paths if some nodes are unused
        #   -----------------------------------------------------------------------------

        node_tree.links.new(textureCoordinatesNode.outputs["UV"], vectorMappingNode.inputs["Vector"])
        
        for inputNodeKey, inputNodeValue in imageTextureNodes.items():
            if(inputNodeValue != "unassigned"):
                node_tree.links.new(vectorMappingNode.outputs["Vector"], inputNodeValue.inputs["Vector"]) #string object has no attribute 'inputs'

        if imageTextureNodes["base_color"] != "unassigned" and imageTextureNodes["ambient_occlusion"] != "unassigned":
            node_tree.links.new(imageTextureNodes["base_color"].outputs["Color"], mixRGBNode.inputs["Color1"])
            node_tree.links.new(imageTextureNodes["ambient_occlusion"].outputs["Color"], mixRGBNode.inputs["Color2"])
            node_tree.links.new(mixRGBNode.outputs["Color"], bsdfNode.inputs[0])

        if imageTextureNodes["base_color"] != "unassigned": 
            node_tree.links.new(imageTextureNodes["base_color"].outputs["Color"], bsdfNode.inputs[0])

        #TODO: Check if should create gamma node
        if imageTextureNodes["roughness"] != "unassigned":
            node_tree.links.new(imageTextureNodes["roughness"].outputs["Color"], gammaNode.inputs["Color"])
            node_tree.links.new(gammaNode.outputs["Color"], bsdfNode.inputs["Roughness"])

        #TODO: Check if should create bump node
        if imageTextureNodes["normal"] != "unassigned":
            node_tree.links.new(imageTextureNodes["normal"].outputs["Color"], bumpNode.inputs["Height"])
            node_tree.links.new(bumpNode.outputs["Normal"], bsdfNode.inputs["Normal"])

                        


        #   ---   ---   ---   ---   ---   ---   ---   ---   ---   ---   ---   ---   ---
        #   Create and Adjust Displacement Modifier
        #   ---   ---   ---   ---   ---   ---   ---   ---   ---   ---   ---   ---   ---


        # Subdivide Modifier
        if context.scene.use_subdivision_modifier_prop:
            if doesNotExist(object.modifiers.get("Subdivide", None)):
                object.modifiers.new("Subdivide", "SUBSURF")

            # Subdivide Settings
            object.modifiers["Subdivide"].levels = context.scene.subdivision_level_prop


        # Displacement Modifier
        if context.scene.use_displacement_modifier_prop:
            if doesNotExist(object.modifiers.get("Displace", None)):
                object.modifiers.new("Displace", "DISPLACE")
            
            # Displacement Settings
            object.modifiers["Displace"].texture = displaceTexture
            object.modifiers["Displace"].texture_coords = "UV"
            object.modifiers["Displace"].texture.extension = "REPEAT"
            object.modifiers["Displace"].texture.repeat_x = context.scene.scale_vectors_prop[0]
            object.modifiers["Displace"].texture.repeat_y = context.scene.scale_vectors_prop[1]
            object.modifiers["Displace"].strength = context.scene.displace_strength_prop


        #   ---   ---   ---   ---   ---   ---   ---   ---   ---   ---   ---   ---   ---
        #   Update Active Material
        #   ---   ---   ---   ---   ---   ---   ---   ---   ---   ---   ---   ---   ---

        # Override Active Material of Active Object
        if Global.addMaterialToObject == 1:
            object.active_material = activeMaterial




        #   ---   ---   ---   ---   ---   ---   ---   ---   ---   ---   ---   ---   ---
        #   Shade All Faces Smooth
        #   ---   ---   ---   ---   ---   ---   ---   ---   ---   ---   ---   ---   ---

        # Shade every Face of every polygon of this object smooth
        if context.scene.shade_smooth_prop == True:
            for f in object.data.polygons:
                        f.use_smooth = True
        



        return {'FINISHED'}




# ------------------------------------------------------------------------
#    Panel
# ------------------------------------------------------------------------

class OBJECT_PT_CustomPanel(Panel):
    bl_idname = "OBJECT_PT_CustomPanel"
    bl_label = Global.panelName
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_category = "Scripts"
    bl_context = "objectmode"   

    @classmethod
    def poll(self,context):
        return context.object is not None

    def draw(self, context):

        # Variables - Context
        layout = self.layout
        object = context.object

        # Variables - Input Texture Buttons
        textureID = 0
        requiredTextures = ["Base Color", "Ambient Occlusion", "Roughness", "Normal", "Displace"]
        optionalTextures = ["Opacity", "Metallic"]

        # Function - Change "OverrideButton" Availability
        enableOverrideMatButton = False
        if object.data.materials:
            if(len(object.material_slots) > 0):
                if object.active_material:
                    enableOverrideMatButton = True

        # ------------------------------------------------------------------------
        #    Draw Area
        # ------------------------------------------------------------------------


        # Base Settings
        #TODO: The header section should be fancier, maybe add a preview window for the new material.
        boxBase = layout.box()
        col1 = boxBase.column(align=True)
        col1.prop(context.scene, "material_name_prop") 
        col1.prop(context.scene, "shade_smooth_prop")
        layout.separator()


        # Input Textures - Required
        #TODO-SHARED: Merge Required with Optional Textures since changing settings and then overriding, does not require updating an image as well
        #TODO-SHARED: Users may be given a clearer choice to rename and to import/remove-from-import and to delete existing textures
        boxInputImages = layout.box()
        boxInputImages.label(text="Basic Textures")
        col2 = boxInputImages.column(align=True)
        for textureName in requiredTextures:
            if getListItemValue(Global.inputImages, textureID) == "unassigned": 
                col2.operator(uploadOperator.bl_idname, text=textureName, icon="IMAGE").id = textureID
            else:
                col2.operator(resetOperator.bl_idname, text="Remove "+textureName, icon="CANCEL").id = textureID
            textureID = textureID + 1
        layout.separator()


        # Input Textures - Optional
        #TODO-SHARED: Merge Required with Optional Textures since changing settings and then overriding, does not require updating an image as well
        #TODO-SHARED: Users may be given a clearer choice to rename and to import/remove-from-import and to delete existing textures
        boxInputImages.label(text="Optional Textures")
        col3 = boxInputImages.column(align=True)
        for textureName in optionalTextures:
            if getListItemValue(Global.inputImages, textureID) == "unassigned": 
                col3.operator(uploadOperator.bl_idname, text=textureName, icon="IMAGE").id = textureID
            else:
                col3.operator(resetOperator.bl_idname, text="Remove "+textureName, icon="CANCEL").id = textureID
            textureID = textureID + 1
        layout.separator()


        # Mapping Node Settings
        #TODO: Only if material contains Mapping Node and at least one Texture, otherwise this setting will have no effect
        boxScaleTextures = layout.box()
        col1 = boxScaleTextures.column(align=True)
        col1.prop(context.scene, "scale_vectors_prop")
        layout.separator()


        # Bump Node Settings
        #TODO: Only if material contains Bump Node and Bump Texture, otherwise this setting will have no effect.
        boxNormal = layout.box()
        col1 = boxNormal.column(align=True)
        col1.prop(context.scene, "use_normal_node_prop")
        col2 = boxNormal.column(align=True)
        col2.enabled = context.scene.use_normal_node_prop
        col2.prop(context.scene, "bump_strength_prop")
        layout.separator()

        
        # Gamma Node Settings
        #TODO: Only if material contains Gamma Node and Roughness Texture, otherwise this setting will have no effect.
        boxOtherSettings = layout.box()
        boxOtherSettings.label(text="Other settings")
        col1 = boxOtherSettings.column(align=True)
        col1.prop(context.scene, "roughness_gamma_prop")
        layout.separator()


        # Subdivision Modifier Settings
        boxSubdivide = layout.box()
        col1 = boxSubdivide.column(align=True)
        col1.prop(context.scene, "use_subdivision_modifier_prop")
        col2 = boxSubdivide.column(align=True)
        col2.enabled = context.scene.use_subdivision_modifier_prop
        col2.prop(context.scene, "subdivision_level_prop")
        layout.separator()


        # Displacement Modifier Settings
        boxDisplace = layout.box()
        col1 = boxDisplace.column()
        col1.prop(context.scene, "use_displacement_modifier_prop")
        col2 = boxDisplace.column()
        col2.enabled = context.scene.use_displacement_modifier_prop
        col2.prop(context.scene, "displace_strength_prop")
        layout.separator()
        

        # Apply Buttons
        #TODO: Maybe consider a checkbox for live-editing, for each of the modes (create new, and override)
        boxApply = layout.box()
        boxApply.label(text="Apply Material")
        col1 = boxApply.column(align=True)
        col1.operator(submitOperator.bl_idname, text="Create New Material", icon="CONSOLE").id = 0  
        col2 = boxApply.column(align=True)
        col2.enabled = enableOverrideMatButton
        col2.operator(submitOperator.bl_idname, text="Override Active Material", icon="CONSOLE").id = 1

        #TODO: Take breaks, have fun ^-^


# ------------------------------------------------------------------------
#    Registration
# ------------------------------------------------------------------------

def register():
    bpy.utils.register_class(OBJECT_PT_CustomPanel)
    bpy.utils.register_class(submitOperator)
    bpy.utils.register_class(resetOperator)
    bpy.utils.register_class(OT_TestOpenFileBrowser)
    bpy.utils.register_class(uploadOperator)
    bpy.types.Scene.use_normal_node_prop = bpy.props.BoolProperty \
      (
        name = "Create Normal Node",
        description = "When this checkbox is ticked, your normal image will be imported and control the bump node in your shader editor",
        default = True
      )
    bpy.types.Scene.use_displacement_modifier_prop = bpy.props.BoolProperty \
      (
        name = "Create Displace modifier",
        description = "When this checkbox is ticked, a new displace modifier will be automaticially added to the selected object and use your displace texture.",
        default = True
      )
    bpy.types.Scene.use_subdivision_modifier_prop = bpy.props.BoolProperty \
      (
        name = "Create Subdivision modifier",
        description = "When this checkbox is ticked, a new subdivsion modifier will be automaticially added to the selected object",
        default = True
      )
    bpy.types.Scene.subdivision_level_prop = bpy.props.IntProperty \
      (
        name = "Subdivision Level",
        description = "Represents the level of the Subdivision modifier applied to this object. (If you already have a finely subdivided mesh, you can toggle subdivision off)",
        default = 7,
        min= 0,
        max=12
      )
    bpy.types.Scene.roughness_gamma_prop = bpy.props.FloatProperty \
      (
        name = "Roughness Gamma",
        description = "Represents the Roughness of the Roughness texture (can be altered in node editor)",
        default = 1.33,
        min = 0.0,
        max = 10.0
      )
    bpy.types.Scene.displace_strength_prop = bpy.props.FloatProperty \
      (
        name = "Displace Strength",
        description = "Represents the Strength of the Displace texture (can be altered in displace modifier)",
        default = 0.15,
        min = 0.0,
        max = 10.0
      )
    bpy.types.Scene.bump_strength_prop = bpy.props.FloatProperty \
      (
          name = "Bump Strength",
          description = "Represents the Bumpiness of the Normal texture (can be altered in node editor)",
          default = 0.30,
          min = 0.0,
          max = 1.0
      )
    bpy.types.Scene.material_name_prop = bpy.props.StringProperty \
      (
        name = "Material name",
        description = "The new name for your material",
        default = "PBR Material"
      )
    bpy.types.Scene.scale_vectors_prop = bpy.props.FloatVectorProperty \
      (
        name = "Scale Texture XYZ",
        description = "Represents the scaling in the ShaderNodes Vector Mapping and the Displacement Texture repeat amount",
        default = (2.0, 2.0, 1.0)
      )
    bpy.types.Scene.shade_smooth_prop = bpy.props.BoolProperty \
      (
        name = "Smooth Shading",
        description = "When ticked, every face will be shaded as smooth",
        default = True
      )

def unregister():
    bpy.utils.unregister_class(OBJECT_PT_CustomPanel)
    bpy.utils.unregister_class(submitOperator)
    bpy.utils.unregister_class(resetOperator)
    bpy.utils.unregister_class(OT_TestOpenFileBrowser)
 
    del bpy.types.Scene.displace_strength_prop
    del bpy.types.Scene.use_normal_node_prop
    del bpy.types.Scene.use_displacement_modifier_prop
    del bpy.types.Scene.scale_vectors_prop 
    del bpy.types.Scene.material_name_prop
    del bpy.types.Scene.shade_smooth_prop
    del bpy.types.Scene.bump_strength_prop
    del bpy.types.Scene.roughness_gamma_prop
    del bpy.types.Scene.use_subdivision_modifier_prop
    del bpy.types.Scene.subdivision_level_prop

if __name__ == "__main__":
    register()